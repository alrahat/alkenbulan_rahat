<?php

  include('header.php');
?>

			<!-- Begin Content -->
			<div class="content-40mg">
				<div class="container">

					<div class="row">

						<!-- Begin Login -->
						<div class="col-sm-3">
							
							   
						</div><!-- /column-->
						<!-- End Login -->

						<!-- Begin Register -->
						<div class="col-sm-6 mt30-xs">
							<div class="panel no-margin panel-default">
							    <div class="panel-heading">Create an account</div>
							    <div class="panel-body">
							        <form role="form" method="POST" action="register_action.php">
							           <div class="form-group">
							                <input type="text" class="form-control" name="fname" id="firstName" placeholder="First Name"  required="required">
							            </div>
							            <div class="form-group">
							                <input type="text" class="form-control" name="lname" id="lastName" placeholder="Last Name"  required="required">
							            </div>
							            <div class="form-group">
							                <div class="input-group">
							                    <div class="input-group-addon"><span class="ion-android-mail" style="font-size:9px;"></span></div>
							                    <input class="form-control" type="email" name="email" placeholder="Enter email"  required="required">
							                </div>
							            </div>
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="ion-ios7-locked"></span></div>
							                	<input type="password" name="password" class="form-control" id="exampleInputPassword3" placeholder="Password"  required="required">
							                </div>
							            </div>
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="ion-ios7-locked"></span></div>
							                	<input type="password" name="cpassword" class="form-control" id="exampleInputPassword4" placeholder="Confirm Password"  required="required">
							                </div>
							            </div>
							            <!-- <div class="checkbox">
							                <label>
							                  <input type="checkbox" required="required"> I read the terms of service.
							                </label>
							            </div> -->
							            <hr class="mb20 mt20">
							            
							            <hr class="mt20 mb20">
							            <button type="submit" name="submit" value="submit" class="btn btn-rw btn-primary">Register</button> &nbsp;&nbsp;&nbsp;<small><a href="#">Terms of service</a></small>
							        </form><!-- /form -->
							    </div><!-- /panel body -->
							</div><!-- /panel -->
						</div><!-- /column-->
						<!-- Begin Login -->
						<div class="col-sm-3">
							
							   
						</div><!-- /column-->
						<!-- End Register -->

					</div><!-- /row -->

				</div><!-- /container -->
			</div><!-- /content -->
			<!-- End Content -->

<?php
  include('footer.php');
?>