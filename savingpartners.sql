-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 03, 2018 at 08:10 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `savingpartners`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(55) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` int(55) NOT NULL DEFAULT '0',
  `status` int(55) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `password`, `type`, `status`) VALUES
(1, 'admin', 'password', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(255) NOT NULL,
  `create_userid` varchar(255) NOT NULL,
  `group_memberid` varchar(255) NOT NULL,
  `group_amount` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_user`
--

CREATE TABLE `message_user` (
  `id` int(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_user`
--

INSERT INTO `message_user` (`id`, `name`, `email`, `mobile`, `subject`, `message`, `created_on`, `status`) VALUES
(1, 'amit kumar', 'amit@gmail.com', '9407133295', 'want to join', 'How this will work ?', '2018-07-14 05:48:07', 0),
(3, 'pooja prajapati', 'a@gmail.com', '9407133295', 'czxdzzs', 'cvxzxdzcsxc', '2018-08-02 00:27:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `period`
--

CREATE TABLE `period` (
  `id` int(255) NOT NULL,
  `period` varchar(255) DEFAULT NULL,
  `type_id` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `period`
--

INSERT INTO `period` (`id`, `period`, `type_id`, `status`) VALUES
(1, '10 week', '2', 0),
(2, '20 week ', '2', 0),
(12, '20', '5', 0),
(4, '1 month', '3', 0),
(6, '2 month', '3', 0),
(7, '3 month', '3', 0),
(8, '4 month', '3', 0),
(9, '5 month', '3', 0),
(10, '15 week', '2', 0),
(11, '10 months', '3', 0),
(13, '10 week', '5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` int(255) NOT NULL,
  `price` varchar(255) DEFAULT NULL,
  `type_id` varchar(255) DEFAULT NULL,
  `period_id` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `price`, `type_id`, `period_id`, `status`) VALUES
(2, '5 ', '2', '1', 0),
(3, '10 ', '3', '6', 0),
(9, '100', '3', '9', 0),
(10, '200', '3', '11', 0),
(4, '1000', '3', '3', 0),
(5, '15', '2', '2', 0),
(6, '5 ', '3', '9', 0),
(11, '100', '5', '13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `register_user`
--

CREATE TABLE `register_user` (
  `id` int(255) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register_user`
--

INSERT INTO `register_user` (`id`, `fname`, `lname`, `email`, `password`, `created_on`, `status`) VALUES
(1, 'amit', 'kumar', 'pawanshrinkcomcs@gmail.com', '123', '2018-06-23 05:47:22', 0),
(6, 'Lofton', 'Adam', 'notfol@hotmail.com', '559849', '2018-06-25 00:41:40', 0),
(3, 'rahul', 'sahu', 'rahul16692@gmail.com', '123456', '2018-06-23 05:47:22', 0),
(4, 'pooja', 'prajapati', 'pooja.ggits@gmail.com', '374272', '2018-06-23 05:47:22', 0),
(5, 'pooja ', 'prajapati', 'pooja.shrinkcom@gmail.com', '809577', '2018-06-23 05:47:22', 0),
(7, 'Lofton', 'Adam', 'notfol@outlook.com', '!tetteh!', '2018-06-30 10:33:31', 0),
(8, 'pooja', 'prajapati', 'shrinkcom1@gmail.com', '1234567890pp', '2018-07-05 00:29:27', 0),
(9, 'pooja', 'prajapati', 'pp021609@gmail.com', '1234567890pp', '2018-07-12 23:46:45', 0),
(10, 'pj', 'prajapati', 'sunil233patel@gmail.com', '1234567890pp', '2018-07-24 06:12:51', 0),
(11, 'Ben', 'Tetteh', 'benjtee@aol.com', 'bella67', '2018-07-28 08:39:05', 0),
(12, 'soniya', 'prajapati', 'sanjay@gmail.com', '123456789', '2018-08-02 00:27:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`, `status`) VALUES
(2, 'Weekly', 0),
(3, 'Monthly', 0),
(5, 'Fort-night', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_selected_plan`
--

CREATE TABLE `user_selected_plan` (
  `id` int(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `planid` varchar(255) DEFAULT NULL,
  `plan_type` varchar(255) DEFAULT NULL,
  `plan_period` varchar(255) DEFAULT NULL,
  `plan_amount` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_selected_plan`
--

INSERT INTO `user_selected_plan` (`id`, `user_id`, `user_name`, `planid`, `plan_type`, `plan_period`, `plan_amount`, `created_on`, `status`) VALUES
(1, '1', 'amit', '2', '3', '6', '10', '2018-06-28 00:27:15', 0),
(4, '5', 'pooja ', '2', '2', '1', '5 ', '2018-06-29 00:08:03', 0),
(3, '1', 'amit', '2', '2', '1', '5', '2018-06-28 02:00:33', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_user`
--
ALTER TABLE `message_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register_user`
--
ALTER TABLE `register_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_selected_plan`
--
ALTER TABLE `user_selected_plan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message_user`
--
ALTER TABLE `message_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `period`
--
ALTER TABLE `period`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `register_user`
--
ALTER TABLE `register_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_selected_plan`
--
ALTER TABLE `user_selected_plan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
