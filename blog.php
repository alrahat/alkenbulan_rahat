<?php

  include('header.php');
?>

			<!-- Begin Content Section -->
			<section class="mt40 mb40">
				<div class="container">
				    <div class="row">

				   		<!-- Blog Posts -->
				    	<div class="col-sm-8">
							<div class="blog-post mb40">
	                        	<div class="view image-hover-1 no-margin">
									<!-- Blog Thumb -->
									<img class="img-responsive" src="images/backgrounds/stock10.jpg" alt="..." />
							        <div class="mask">
								        <div class="image-hover-content">
								       		<!-- Zoom + Blog Link -->
											<a href="images/projects/illustration1.jpg" class="info image-zoom-link">
												<div class="image-icon-holder"><span class="ion-ios7-search image-icons"></span></div>
											</a>
											<a href="portfolio-single-project.html" class="info">
												<div class="image-icon-holder"><span class="ion-link image-icons"></span></div>
											</a>
										</div><!-- /image hover content -->
									</div><!-- /mask-->
								</div><!-- /image hover -->
		                        <div class="blog-post-holder">
		                            <ul class="list-inline posted-info">
		                                <li>By <a href="#">Denis Samardjiev</a></li>
		                                <li>In <a href="#">Design</a></li>
		                                <li>Posted January 1, 2015</li>
		                            </ul>
		                            <hr align="left" class="mt15 mb10" style="width:50px;">
		                            <h2><a href="#">This is the offical post of the blog</a></h2>
		                            <p>Nullam elementum tincidunt massa, a pulvinar leo ultricies ut. Ut fringilla lectus tellus, imperdiet molestie est volutpat at. Sed viverra cursus nibh, sed consectetur ipsum sollicitudin sed. Cras erat est, euismod id congue sed, sollicitudin sed odio. Nullam non metus in mi rhoncus efficitur...</p>
		                            <a href="#" class="btn btn-rw btn-primary mt10">Read more</a>
		                        </div>                
		                    </div>
		                    <div class="blog-post mb40">
	                        	<div class="view image-hover-1 no-margin">
									<!-- Blog Thumb -->
									<img class="img-responsive" src="images/backgrounds/stock11.jpg" alt="..." />
							        <div class="mask">
								        <div class="image-hover-content">
								       		<!-- Zoom + Blog Link -->
											<a href="images/projects/illustration1.jpg" class="info image-zoom-link">
												<div class="image-icon-holder"><span class="ion-ios7-search image-icons"></span></div>
											</a>
											<a href="portfolio-single-project.html" class="info">
												<div class="image-icon-holder"><span class="ion-link image-icons"></span></div>
											</a>
										</div><!-- /image hover content -->
									</div><!-- /mask-->
								</div><!-- /image hover -->
		                        <div class="blog-post-holder">
		                            <ul class="list-inline posted-info">
		                                <li>By <a href="#">Denis Samardjiev</a></li>
		                                <li>In <a href="#">Design</a></li>
		                                <li>Posted January 1, 2015</li>
		                            </ul>
		                            <hr align="left" class="mt15 mb10" style="width:50px;">
		                            <h2><a href="#">Another post about something, seaguls</a></h2>
		                            <p>Nullam elementum tincidunt massa, a pulvinar leo ultricies ut. Ut fringilla lectus tellus, imperdiet molestie est volutpat at. Sed viverra cursus nibh, sed consectetur ipsum sollicitudin sed. Cras erat est, euismod id congue sed, sollicitudin sed odio. Nullam non metus in mi rhoncus efficitur...</p>
		                            <a href="#" class="btn btn-rw btn-primary mt10">Read more</a>
		                        </div>                
		                    </div>
		                    <div class="blog-post mb40">
	                        	<div class="view image-hover-1 no-margin">
									<!-- Blog Thumb -->
									<img class="img-responsive" src="images/backgrounds/stock12.jpg" alt="..." />
							        <div class="mask">
								        <div class="image-hover-content">
								       		<!-- Zoom + Blog Link -->
											<a href="images/projects/illustration1.jpg" class="info image-zoom-link">
												<div class="image-icon-holder"><span class="ion-ios7-search image-icons"></span></div>
											</a>
											<a href="portfolio-single-project.html" class="info">
												<div class="image-icon-holder"><span class="ion-link image-icons"></span></div>
											</a>
										</div><!-- /image hover content -->
									</div><!-- /mask-->
								</div><!-- /image hover -->
		                        <div class="blog-post-holder">
		                            <ul class="list-inline posted-info">
		                                <li>By <a href="#">Denis Samardjiev</a></li>
		                                <li>In <a href="#">Design</a></li>
		                                <li>Posted January 1, 2015</li>
		                            </ul>
		                            <hr align="left" class="mt15 mb10" style="width:50px;">
		                            <h2><a href="#">This is the offical post of the blog</a></h2>
		                            <p>Nullam elementum tincidunt massa, a pulvinar leo ultricies ut. Ut fringilla lectus tellus, imperdiet molestie est volutpat at. Sed viverra cursus nibh, sed consectetur ipsum sollicitudin sed. Cras erat est, euismod id congue sed, sollicitudin sed odio. Nullam non metus in mi rhoncus efficitur...</p>
		                            <a href="#" class="btn btn-rw btn-primary mt10">Read more</a>
		                        </div>                
		                    </div>
							<nav class="text-center">
								<ul class="pagination no-margin">
									<li>
										<a href="#" aria-label="Previous">
											<span aria-hidden="true">&laquo;</span>
										</a>
									</li>
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li>
										<a href="#" aria-label="Next">
											<span aria-hidden="true">&raquo;</span>
										</a>
									</li>
								</ul>
							</nav>
				        </div>

				        <!-- Sidebar -->
				        <div class="col-sm-4">
				        	<div class="content-box content-box-bordered mt30-xs mb30">
						        <span class="ion-beaker bordered-icon-color bordered-icon-sm"></span>
						        <h4 class="pt15">Our Blog</h4>
						        <p class="no-margin">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. Integer a elit turpis. Phasellus non varius mi. Nullam elementum tincidunt massa, a pulvinar leo ultricies ut.</p>
						    </div>

						    <div class="blog-heading"><h3>Navigate</h3></div>
							<ul class="nav nav-pills nav-stacked mb30">
								<li><a href="index.html">Home</a></li>
								<li class="active"><a href="blog-posts-1.html">Blog</a></li>
								<li><a href="portfolio-fixed-3-columns-circle.html">Our Work</a></li>
								<li><a href="portfolio-fixed-about-us-team.html">The Team</a></li>
								<li><a href="pages-contact-1.html">Contact</a></li>
							</ul>

				        	<div class="blog-heading"><h3>About</h3></div>
							<p class="mb30">Vivamus quis est a metus tincidunt viverra. Morbi condimentum tempor libero at cursus. Etiam sollicitudin venenatis arcu id vulputate. Phasellus in pharetra lorem, non suscipit mauris. Cras erat est, euismod id congue sed, sollicitudin sed odio. Nullam non metus in mi rhoncus efficitur...</p>

				        	<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
							    <li class="active"><a href="#posts" role="tab" data-toggle="tab">Posts</a></li>
							    <li><a href="#info" role="tab" data-toggle="tab">Info</a></li>
							</ul>
							<div class="tab-content tab-default mb30">
							    <div class="tab-pane in active" id="posts">
		                            <ul class="media-list">
		                                <li class="media">
		                                    <a class="pull-left" href="#"><img class="media-object" src="http://lorempixel.com/output/city-q-c-80-80-5.jpg" width="80" height="80" alt="image"></a>
		                                    <div class="media-body">
		                                        <p class="media-heading"><a href="#">Lorem ipsum dolor sit amet aut consectetur adipisicing elitl libero</a></p>
		                                        <small>5 Jan, 2015<br><em><a href="#">Web,</a> <a href="#">Webdesign</a></em></small>
		                                    </div>
		                                </li>
		                                <li class="media">
		                                    <a class="pull-left" href="#"><img class="media-object" src="http://lorempixel.com/output/city-q-c-80-80-7.jpg" width="80" height="80" alt="image"></a>
		                                    <div class="media-body">
		                                        <p class="media-heading"><a href="#">Lorem ipsum dolor sit amet in consectetur adipisicing</a></p>
		                                        <small>17 Jan, 2015<br><em><a href="#">Artificial Intelligence</a></em></small>  
		                                    </div>
		                                </li>
		                                <li class="media">
		                                    <a class="pull-left" href="#"><img class="media-object" src="http://lorempixel.com/output/city-q-c-80-80-8.jpg" width="80" height="80" alt="image"></a>
		                                    <div class="media-body">
		                                        <p class="media-heading"><a href="#">Sit amet consectetur adipisicing elit incidunt minus</a></p>
		                                        <small>23 Jan, 2015<br><em><a href="#">Art,</a> <a href="#">Lifestyles</a></em></small>   
		                                    </div>
		                                </li>
		                            </ul>
							    </div>
							    <div class="tab-pane" id="info">Donec tristique dignissim nisi, quis fermentum quam. Sed turpis ipsum, tempor vel malesuada ac, pharetra eu nunc. Aliquam condimentum mauris eget justo aliquam, in rhoncus libero egestas.
							    <a href="pages-about-1.html" class="btn btn-rw btn-primary btn-sm mt10">About</a></div>
							</div>
							<!-- End Nav tabs -->

				        	<div class="blog-heading"><h3>Recent Posts</h3></div>
							<ul class="list-unstyled latest-posts">
		                        <li>
		                            <h3 class="no-margin"><a href="#">Wireframe for the news view...</a></h3>
		                            <small>5 Jan, 2015 / <a href="#">Web,</a> <a href="#">Webdesign</a></small>
		                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam odio leo.</p>
		                        </li>
		                        <li>
		                            <h3 class="no-margin"><a href="#">It is a long established fact that a reader</a></h3>
		                            <small>17 Jan, 2015 / <a href="#">Artificial Intelligence</a></small>                            
		                            <p>Pellentesque efficitur blandit dui, porta cursus velit imperdiet sit amet.</p>
		                        </li>
		                        <li>
		                            <h3 class="no-margin"><a href="#">The point of using Lorem Ipsum</a></h3>
		                            <small>19 Jan, 2015 / <a href="#">Hi-Tech,</a> <a href="#">Technology</a></small>                            
		                            <p>Phasellus ullamcorper pellentesque ex. Cras venenatis elit orci, vitae dictum elit egestas a. Nunc nec auctor mauris, semper scelerisque nibh.</p>
		                        </li>
		                        <li>
		                            <h3 class="no-margin"><a href="#">Many desktop publishing packages...</a></h3>
		                            <small>23 Jan, 2015 / <a href="#">Art,</a> <a href="#">Lifestyles</a></small>                            
		                            <p class="no-border">Integer vehicula sed justo ac dapibus. In sodales nunc non varius accumsan.</p>
		                        </li>
		                    </ul>

							<div class="blog-heading"><h3>Tags</h3></div>
							<p class="mb30"><a href="#" class="btn btn-rw btn-primary tags btn-sm">Technology</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Abstract</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Architecture</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Web Design</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Travel</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Illustration</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Diagram</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Random</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Economy</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Viewpoint</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Art</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Moscato</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Spacemen</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Artistic</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Programming</a><a href="#" class="btn btn-rw btn-primary tags btn-sm">Ability</a></p>
		                </div>
				    </div>
			    </div>
			</section><!-- /content -->
			<!-- End Content Section -->

<?php 
  include('footer.php');
?>