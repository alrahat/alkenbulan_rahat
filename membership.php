<?php
  include('header.php');
?>
        
      
   
</div>
<section style="background-image:url(./images/membership-banner.jpg);">
	<h1 style="text-align:center; padding: 100px; color:#fff;">Membership</h1>
</section>	


<section style="padding:50px;">
    <div class="divider-line solid light"></div>
    <div class="container-fluid">
      <div class="row sec-padding">
        
        <div class="col-md-11 col-centered">
		<div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 margin-bottom"> <img src="ourhands.jpg" style="
    height: 500px;
"alt="" class="img-responsive"> </div>
        <!--end item-->
        
        <div class="col-md-8 col-sm-6 col-xs-12">
          <div class="text-box white padding-4 margin-bottom">
            <h1 class="uppercase font-weight-b">ABOUT MEMBERSHIP</h1>
			<h3>WHO CAN JOIN?</h3>
<p>Alkebulan Savings Club (ASC) was founded by a passionate and an aspiring entrepreneur who have been consistently seeking alternative ways to improve peoples financial lives. ASC presents an alternative saving scheme to that provided by the high street banks. 
Everyone stands to benefit by joining ASC.
<p><i class="fa fa-check"></i> Group of friends, family members, colleagues who want to do collective savings.</p>
<p><i class="fa fa-check"></i> Anyone who want to save towards a special occasion or for that special buy.</p>

<p><i class="fa fa-check"></i> Families with low income earnings who are not likely to get loans from the main stream banks .</p>

<p><i class="fa fa-check"></i> Those who cannot afford the high interest charges by bank, or</p>

<p><i class="fa fa-check"></i> Those who just want to invest in their families future.</p>
<div class="clearfix">&nbsp;</div>
<h2>JOIN NOW </h2>
<h4 class="raleway opacity-9 "> <i class="fa fa-check"></i>Join the A-plan.</h4>  
            
            <p>ASC is an online secure and efficient way to save money for a target period. For few minutes registration and a small one-off fee, the member would regularly deposit the agreed amount into their A-Plan account for the prescribed. Please note that this scheme is to empower customers to achieve an intended target therefore savings ONLY become available for withdrawal once the target is attained. <a href="register.php">Please click here to register</a>
</p>
       <h4 class="raleway opacity-9 "> <i class="fa fa-check"></i>Join the S-plan.</h4>  
        <p>Our S-Plan is a reliable rotating savings plan mainly because you are working together with friends or family. Trustworthiness is key when saving and that is exactly what you get with the S-Plan. With our S-plan, you are basically partnering with other trustworthy people known to you to form a group, Individuals in the group save money together and take turn to receive the total savings till each member gets its due.<a href="register.php"> Please click here to register</a>
</p>

<h3>BENEFITS
</h3>

<p>At ASC, we are committed to helping our members get what you want without using credit cards with its associated high costs.
Our A-plan saving scheme helps members to overcome the temptation of withdrawing accounts for unplanned expenses.
Our schemes help members avoid exorbitant and unending loan and interests repayments Members are able to take control of their finances
Our S-plan brings together family members and friends to form a partnership to save  collectively. This scheme therefore unites people and enhance socialisation even if the group members are miles apart.
Our S-Plan Is a rotating savings scheme which helps people raise capital. These people may have limited access to credit or poor credit rating.
 Our S-Plan offers a full credit arrangement for the member of the group who gets this first collections without the payment of any interest..
Members pay no interest on savings, and no interest is paid credit (that is receiving your collections early). Additionally, all transactions are done online.
</p>
<h3>Alkebulan Plan must be your choice of saving club if you want to be in control over your own finances without the stress of opening an account and without the temptation of early withdrawal </h3>
        </div>
        <!--end item-->
        </div>
         <!--end main col-->
		 </div>
      </div>
    </div>
  </div></section>

<?php
  include('footer.php');
?>