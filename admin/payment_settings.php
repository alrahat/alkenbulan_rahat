﻿﻿ <?php
include('php_action/conn.php');
// $jokesid   = $_GET['jokes'];
?>

<?php

include('header.php');
include('../Settings.php');

?>

<?php

$settings = new Settings();

$stripe_settings = $settings->prepared_stripe_settings();

$test_mode = null;
$tsk = null;
$tpk = null;
$lsk = null;
$lpk = null;
$signing_secret = null;

$sk = $settings->get_stripe_secret_key();
$pk = $settings->get_stripe_public_key();


if (!empty($stripe_settings)) {
    $test_mode = isset($stripe_settings['test_mode']) ? $stripe_settings['test_mode'] : null;
    $tsk = isset($stripe_settings['tsk']) ? $stripe_settings['tsk'] : null;
    $tpk = isset($stripe_settings['tpk']) ? $stripe_settings['tpk'] : null;
    $lsk = isset($stripe_settings['lsk']) ? $stripe_settings['lsk'] : null;
    $lpk = isset($stripe_settings['lpk']) ? $stripe_settings['lpk'] : null;
    $signing_secret = isset($stripe_settings['signing_secret']) ? $stripe_settings['signing_secret'] : null;
}


if (isset($_POST)) {

    foreach ($_POST as $settings_key => $settings_val) {
        $settings->setSettings('stripe',$settings_key,$settings_val);
    }
}

?>

<!-- /. NAV SIDE  -->
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">
                    Payment settings
                    <small></small>
                </h1>
            </div>
        </div>
        <!-- /. ROW  -->
        <?php
        if (isset($_GET['payment_settings_updated'])) {  // print_r($_GET);die;
            echo '<div class="alert alert-primary alert-dismissable fade in" role="alert" style="color:Green; font-weight:bold;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                "Payment Settings Updated" </div>';

        }
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Plan
                    </div>
                    <div class="box-body">
                        <form action="payment_settings.php" id="settings-form" class="form-horizontal"
                              method="post">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Test mode</label>
                                    <div class="col-md-6">
                                        <select name="test_mode" class="form-control">
                                            <option value="">--Select mode--</option>
                                            <option value="on" <?= $test_mode == "on" ? " selected " : "" ?> >Test
                                            </option>
                                            <option value="off" <?= $test_mode == "off" ? " selected " : "" ?> >Live
                                            </option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Test secret key</label>
                                    <div class="col-md-6">
                                        <input name="tsk" placeholder="Enter test secret key" class="form-control"
                                               value="<?= $tsk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Test public key</label>
                                    <div class="col-md-6">
                                        <input name="tpk" placeholder="Enter test public key" class="form-control"
                                               value="<?= $tpk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Live secret key</label>
                                    <div class="col-md-6">
                                        <input name="lsk" placeholder="Enter live secret key" class="form-control"
                                               value="<?= $lsk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Live public key</label>
                                    <div class="col-md-6">
                                        <input name="lpk" placeholder="Enter live public key" class="form-control"
                                               value="<?= $lpk ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Signing secret</label>
                                    <div class="col-md-6">
                                        <input name="signing_secret" placeholder="Enter signing secret for webhook"
                                               class="form-control"
                                               value="<?= $signing_secret ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success btn-lg btn-block">Set</button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>


                            </div>
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <footer><p>All right reserved. Template by: <a href="">Shrinkcom software </a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- Bootstrap Js -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Metis Menu Js -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- Custom Js -->
<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="assets/js/morris/morris.js"></script>


<script src="assets/js/custom-scripts.js"></script>

<script src="assets/js/jquery-1.10.2.js"></script>
<!-- DATA TABLE SCRIPTS -->


<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

</script>
</body>
</html>
