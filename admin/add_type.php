﻿<?php
include('php_action/conn.php');
// $jokesid   = $_GET['jokes'];
?>
<?php

include('header.php');

?>

<!-- /. NAV SIDE  -->
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">
                    Add Type
                    <small></small>
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Type
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <?php
                            if (isset($_GET['addtype'])) {  // print_r($_GET);die;
                                echo '<div class="alert alert-primary alert-dismissable fade in" role="alert" style="color:Green; font-weight:bold;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                "Add add type successfully!" </div>';

                            }
                            ?>
                            <div class="col-lg-12">
                                <form role="form" action="add_type_action.php" method="POST"
                                      enctype="multipart/form-data">

                                    <div class="form-group">
                                        <label>Select Plan Group</label>
                                        <select name="plan_group" class="form-control">

                                            <option>Select Plan Group</option>
                                            <?php
                                            $sql = "SELECT * FROM `saving_plan`";

                                            if ($result = mysqli_query($conn, $sql)) {
                                                While ($row = mysqli_fetch_assoc($result)) {
                                                    ?>


                                                    <option value="<?php echo $row['saving_plan_id']; ?>"><?php echo $row['saving_plan_name']; ?> </option>

                                                    <?php
                                                }
                                            }

                                            ?>

                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label> Type</label>
                                        <input class="form-control" type="text" name="type" placeholder="Enter Type"
                                               required="required">

                                    </div>


                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit
                                        Button
                                    </button>

                                </form>
                            </div>


                        </div>

                    </div>

                </div>

            </div>

        </div>


        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Type
                    </div>
                    <div class="panel-body">
                        <?php
                        if (isset($_GET['yes_del'])) {  // print_r($_GET);die;
                            echo '<div class="alert alert-success alert-dismissable fade in" role="alert" style="color:Green; font-weight:bold;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                "Delete Type successfully!" </div>';

                        }
                        if (isset($_GET['sucmsg'])) {  // print_r($_GET);die;
                            echo '<div class="alert alert-success alert-dismissable fade in" role="alert" style="color:Green; font-weight:bold;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                "Edit Type successfully!" </div>';

                        }
                        ?>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Plan Group</th>

                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?PHP
                                $i = 1;
                                $sell = "SELECT * FROM `type` JOIN saving_plan ON type.id_saving_plan =saving_plan.saving_plan_id";
                                $sell_q = mysqli_query($conn, $sell);
                                while ($sql_fetch = mysqli_fetch_assoc($sell_q)) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <th><?php echo $sql_fetch['type']; ?></th>
                                        <th><?php echo $sql_fetch['saving_plan_name']; ?></th>

                                        <td>
                                            <a href="edit_type.php?id=<?php echo $sql_fetch['id']; ?>"><input
                                                        type="submit" name="" value="Edit" class="btn btn-primary"/></a>

                                            <!--  <a href="delete_type.php?idt=<?php echo $sql_fetch['id']; ?>"  onclick="return confirm('Are you sure want to delete?');"><input type="submit" name="" value="Delete" class="btn btn-danger"/></a>-->
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }

                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>


        <footer><p>All right reserved. Template by: <a href="">Shrinkcom software </a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- Bootstrap Js -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Metis Menu Js -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- Custom Js -->
<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="assets/js/morris/morris.js"></script>


<script src="assets/js/custom-scripts.js"></script>

<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

</script>
</body>
</html>
