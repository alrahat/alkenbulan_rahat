var manageMenuTable;

$(document).ready(function() {
	// top bar active
	$('#navBrand').addClass('active');
	//alert();
	// manage brand table
	manageMenuTable = $("#manageMenuTable").DataTable({
		'ajax': 'php_action/fetchmenu.php',
		'order': []		
	});

	// submit brand form function
	$("#submitMenuForm").unbind('submit').bind('submit', function() {
		// remove the error text
		$(".text-danger").remove();
		// remove the form error
		$('.form-group').removeClass('has-error').removeClass('has-success');			

		var menuName  = $("#menuName").val();
		var menuImg = $("#menuImg").val();

		if(menuName  == "") {
			$("#menuName ").after('<p class="text-danger">Menu is required</p>');
			$('#menuName ').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#menuName ").find('.text-danger').remove();
			// success out for form 
			$("#menuName ").closest('.form-group').addClass('has-success');	  	
		}

		if(menuImg == "") {
			$("#menuImg").after('<p class="text-danger">Image is required</p>');

			$('#menuImg').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#menuImg").find('.text-danger').remove();
			// success out for form 
			$("#menuImg").closest('.form-group').addClass('has-success');	  	
		}

		if(menuName && menuImg) {
			var form = $(this);
			var formData = new FormData(this);
			// button loading
			$("#createMenuBtn").button('loading');

			$.ajax({
				url : form.attr('action'),
				type: form.attr('method'),
				data: formData,
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				success:function(response) {
					// button loading
					$("#createMenuBtn").button('reset');

					if(response.success == true) {
						// reload the manage member table 
						manageMenuTable.ajax.reload(null, false);						

  	  			// reset the form text
						$("#submitMenuForm")[0].reset();
						// remove the error text
						$(".text-danger").remove();
						// remove the form error
						$('.form-group').removeClass('has-error').removeClass('has-success');
  	  			
  	  			$('#add-menu-messages').html('<div class="alert alert-success">'+
            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
          '</div>');

  	  			$(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					}  // if

				} // /success
			}); // /ajax	
		} // if

		return false;
	}); // /submit brand form function

});

function removemenu(menuId = null) {
	if(menuId) {
		// remove product button clicked
		$("#removemenuModalBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#removemenuBtn").button('loading');
			$.ajax({
				url: 'php_action/removemenu.php',
				type: 'post',
				data: {menuId: menuId},
				dataType: 'json',
				success:function(response) {
					// loading remove button
					$("#removemenuBtn").button('reset');
					if(response.success == true) {
						// remove product modal
						$("#removemenuModal").modal('hide');

						// update the product table
						manageMenuTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeProductMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function
			}); // /ajax fucntion to remove the product
			return false;
		}); // /remove product btn clicked
	} // /if productid
} // /remove product function

function clearForm(oForm) {
	// var frm_elements = oForm.elements;									
	// console.log(frm_elements);
	// 	for(i=0;i<frm_elements.length;i++) {
	// 		field_type = frm_elements[i].type.toLowerCase();									
	// 		switch (field_type) {
	// 	    case "text":
	// 	    case "password":
	// 	    case "textarea":
	// 	    case "hidden":
	// 	    case "select-one":	    
	// 	      frm_elements[i].value = "";
	// 	      break;
	// 	    case "radio":
	// 	    case "checkbox":	    
	// 	      if (frm_elements[i].checked)
	// 	      {
	// 	          frm_elements[i].checked = false;
	// 	      }
	// 	      break;
	// 	    case "file": 
	// 	    	if(frm_elements[i].options) {
	// 	    		frm_elements[i].options= false;
	// 	    	}
	// 	    default:
	// 	        break;
	//     } // /switch
	// 	} // for
}