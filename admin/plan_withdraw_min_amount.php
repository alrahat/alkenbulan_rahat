﻿ <?php
include('php_action/conn.php');
// $jokesid   = $_GET['jokes'];
?>

<?php

include('header.php');
include('../Settings.php');

?>

<?php
 
 $admin_min_amount_sql = mysqli_query($conn,"SELECT * FROM admin_withdraw_min_amount ORDER BY id DESC LIMIT 1");
$admin_min_amount_row = mysqli_fetch_assoc($admin_min_amount_sql);
$min_amount = $admin_min_amount_row['min_amount'];


if(isset($_POST['submit'])){
    //$admin_charge = $_POST['admin_charge'];
    $edit_min_amount_sql = mysqli_query($conn,"UPDATE admin_withdraw_min_amount SET min_amount = '".$_REQUEST['min_amount']."'");
    
    echo '<script type="text/javascript">
           window.location = "plan_withdraw_min_amount.php?successin=success"
      </script>';   
}
?>

<!-- /. NAV SIDE  -->
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <!-- <h1 class="page-header">
                    Plan Withdraw Charge
                    <small></small>
                </h1> -->
            </div>
        </div>
        <!-- /. ROW  -->
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Plan Withdraw Min Balance
                    </div>
                    <div class="box-body">
                        <br><br>
                        <form action="plan_withdraw_min_amount.php" id="settings-form" class="form-horizontal"
                              method="post">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Min Balance To Withdraw</label>
                                    <div class="col-md-6">
                                        <input name="min_amount" required placeholder="Min Balance to Withdraw" class="form-control"
                                               value="<?php echo $min_amount; ?>"
                                               type="text">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <button type="submit" name="submit" class="btn btn-success btn-small">Set</button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>


                            </div>
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <footer><p>All right reserved. Template by: <a href="">Shrinkcom software </a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- Bootstrap Js -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Metis Menu Js -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- Custom Js -->
<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="assets/js/morris/morris.js"></script>


<script src="assets/js/custom-scripts.js"></script>

<script src="assets/js/jquery-1.10.2.js"></script>
<!-- DATA TABLE SCRIPTS -->


<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

</script>
</body>
</html>
