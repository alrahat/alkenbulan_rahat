<?php
include('php_action/conn.php');
// $jokesid   = $_GET['jokes'];
?>
<?php

include('header.php');

?>

<!-- /. NAV SIDE  -->
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">
                    All User Payments
                    <small></small>
                </h1>
            </div>
        </div>
        <!-- /. ROW  -->


        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        All User Payments
                    </div>
                    <div class="panel-body">
                        <?php
                        if (isset($_GET['yes_del'])) {  // print_r($_GET);die;
                            echo '<div class="alert alert-success alert-dismissable fade in" role="alert" style="color:Green; font-weight:bold;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                "Delete User successfully!" </div>';

                        }
                        if (isset($_GET['sucmsg'])) {  // print_r($_GET);die;
                            echo '<div class="alert alert-success alert-dismissable fade in" role="alert" style="color:Green; font-weight:bold;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                "Edit Joke Category successfully!" </div>';

                        }
                        ?>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Amount</th>
                                    <th>Plan Type</th>
                                    <th>Plan Period</th>
                                    <!--   <th>Plan id</th>  -->
                                    <th>Created on</th>


                                </tr>
                                </thead>
                                <tbody>
                                <?PHP


                                $i = 1;
                                // $sell =
                                //     "SELECT user_selected_plan.id, user_selected_plan.user_id, user_selected_plan.user_name, user_selected_plan.planid, user_selected_plan.plan_type, user_selected_plan.plan_period, user_selected_plan.plan_amount, user_selected_plan.created_on, user_selected_plan.status,register_user.id as userid, register_user.fname, register_user.lname, register_user.email, register_user.password, register_user.created_on, register_user.status,period.id, period.period, period.type_id, period.status,type.id, type.type, type.status 
                                //     FROM user_selected_plan INNER JOIN register_user ON user_selected_plan.user_id = register_user.id 
                                //     INNER JOIN period ON period.id = user_selected_plan.plan_period
                                //     INNER JOIN type ON type.id = user_selected_plan.plan_type";



                                //Rahat Starts
                                 $sell = "SELECT stripe_payment.id, stripe_payment.user_id,stripe_payment.plan_id,stripe_payment.price,stripe_payment.payment_date,stripe_payment.total_cycle,plan.id as plan_id,plan.type_id,plan.period_id,type.id as type_id,type.type,period.id,period.period,register_user.id as user_id,register_user.fname,register_user.lname,register_user.email FROM `stripe_payment` INNER JOIN plan ON plan.id = stripe_payment.plan_id INNER JOIN type ON type.id = plan.type_id INNER JOIN period ON period.id = plan.period_id INNER JOIN register_user ON register_user.id = stripe_payment.user_id ";


                                /*mahmud <starts>*/

                                /*$sell =
                                    "SELECT user_selected_plan.id,
                                    user_selected_plan.user_id,
                                    user_selected_plan.user_name,
                                    user_selected_plan.planid,
                                    user_selected_plan.plan_type,
                                    user_selected_plan.plan_period,
                                    user_selected_plan.plan_amount, user_selected_plan.created_on,
                                    user_selected_plan.status,register_user.id as userid, register_user.fname,
                                    register_user.lname, register_user.email, register_user.password,         
                                    register_user.created_on, register_user.status,
                                    period.id, 
                                    period.period,
                                    period.type_id, 
                                    period.status,
                                    type.id, 
                                    type.type, 
                                    type.status
                                     
                                    FROM user_selected_plan 
                                    INNER JOIN period ON period.id = user_selected_plan.plan_period
                                    INNER JOIN type ON type.id = user_selected_plan.plan_type
                                    LEFT JOIN register_user ON user_selected_plan.user_id = register_user.id 
                                    ";*/
                                
                                /*mahmud <ends>*/

                                $sell_q = mysqli_query($conn, $sell);


                                $sell_q = mysqli_query($conn, $sell);
                                while ($sql_fetch = mysqli_fetch_assoc($sell_q)) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $i; ?></td>
                                        <th><?php echo $sql_fetch['fname']; ?> <?php echo $sql_fetch['lname']; ?></th>
                                        <td><?php echo $sql_fetch['email'];?></td>
                                        <td><?php echo $sql_fetch['price']; ?></td>
                                        <td><?php echo $sql_fetch['type']; ?></td>
                                        <td><?php echo $sql_fetch['period']; ?></td>
                                        <!--    <td><?php echo $sql_fetch['planid']; ?></td>  -->
                                        <td><?php echo $sql_fetch['payment_date']; ?></td>

                                        <!--    <td>
                              <a href="edit_user.php?id=<?php echo $sql_fetch['id']; ?>"><input type="submit" name="" value="Edit" class="btn btn-primary"/></a>
                                  
                               <a href="delete_user.php?id=<?php echo $sql_fetch['id']; ?>"  onclick="return confirm('Are you sure want to delete?');"><input type="submit" name="" value="Delete" class="btn btn-danger"/></a>
                            </td>  -->
                                    </tr>
                                    <?php
                                    $i++;
                                }

                                ?>
                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>


        <footer><p>All right reserved. Template by: <a href="">Shrinkcom software </a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- Bootstrap Js -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Metis Menu Js -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- Custom Js -->
<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="assets/js/morris/morris.js"></script>


<script src="assets/js/custom-scripts.js"></script>


<script src="assets/js/jquery-1.10.2.js"></script>


<!-- DATA TABLE SCRIPTS -->
<script src="assets/js/dataTables/jquery.dataTables.js"></script>
<script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });
</script>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);

</script>
</body>
</html>
