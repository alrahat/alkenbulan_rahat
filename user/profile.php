<?php include('header.php'); ?>

    <div class="page-wrapper">   
<?php

if(isset($_GET['msg1']))
{
	echo '<div class="alert alert-warning">
  <strong>Current Password does not matched!</strong> 
</div>';
	
}
if(isset($_GET['msg2']))
{
	echo '<div class="alert alert-warning">
  <strong> New Password And Confirm Password not matched</strong> 
</div>';
	
}
if(isset($_GET['msg3']))
{
	echo '<div class="alert alert-success">
  <strong>Updated Successfully</strong> 
</div>';
	
}

       $query = mysqli_query($conn,"SELECT * FROM `register_user` WHERE id='".$_SESSION['user_id']."'");

      $row = mysqli_fetch_assoc($query);
  // print_r($row);
 ?>	
<div class="container">
  <div class="col-md-12 " style="margin-top:60px; margin-bottom:60px;">
   <form class="form-horizontal" method="POST" action="../profile.php">
    <fieldset>

<!-- Form Name -->
<legend>Update Profile</legend>

  <div class="form-group">
  <label class="col-md-4 control-label" for="Name">First Name</label>  
  <div class="col-md-4">
 <div class="input-group">
       
       <input  name="fname" type="text" placeholder="First Name"  value="<?php echo $row['fname'];?>" class="form-control input-md" required="required">
      </div>
  </div>
</div>

  
  <div class="form-group">
  <label class="col-md-4 control-label" for="Name">Last Name</label>  
  <div class="col-md-4">
 <div class="input-group">
       
       <input  name="lname" type="text" placeholder="Last Name"   value="<?php echo $row['lname'];?>" class="form-control input-md" required="required">
      </div>
  </div>
</div>
  <div class="form-group">
  <label class="col-md-4 control-label" for="Name">Email</label>  
  <div class="col-md-4">
 <div class="input-group">
       
       <input  name="email" type="email" placeholder="Email"   value="<?php echo $row['email'];?>" class="form-control input-md" required="required">
      </div>
  </div>
</div>
  <div class="form-group">
  <label class="col-md-4 control-label" for="Name">Address</label>  
  <div class="col-md-4">
 <div class="input-group">
       
       <input  name="address" type="text" placeholder="address"   value="<?php echo $row['address'];?>" class="form-control input-md" required="required">
      </div>
  </div>
</div>
  <div class="form-group">
  <label class="col-md-4 control-label" for="Name">Phone</label>  
  <div class="col-md-4">
 <div class="input-group">
       
       <input  name="phone" type="text" placeholder="Phone"  class="form-control input-md"  value="<?php echo $row['phone'];?>" required="required">
      </div>
  </div>
</div>
 <div class="form-group">
  <label class="col-md-4 control-label" for="Name">Next of kin formula</label> 
  
      <div class="col-md-4">
 <div class="input-group">
       
       <input  name="nominy_name" type="text" placeholder="Enter a Next of kin Name"  class="form-control input-md"  value="<?php echo $row['nominy_name'];?>" >
      </div>     </div> 
      <br/>
      <div class="col-md-4">
 <div class="input-group">
       
       <input  name="nominy_phone" type="text" placeholder="Enter a Next of kin Phone Number"  class="form-control input-md"  value="<?php echo $row['nominy_phone'];?>" >
      </div>     </div> 
      <br/>
  <div class="col-md-4">
 <div class="input-group">
       
<select class="form-control input-md" name="relation" >
<option value="">Select Relation</option>
<option value="Mother" <?php if($row['relation']=='Mother') { echo 'selected'; } ?>> Mother</option>

<option value="Father" <?php if($row['relation']=='Father') { echo 'selected'; } ?>>Father</option>
<option value="Sister" <?php if($row['relation']=='Sister') { echo 'selected'; } ?>>Sister</option>
<option value="Brother" <?php if($row['relation']=='Brother') { echo 'selected'; } ?>>Brother</option>

<option value="Wife" <?php if($row['relation']=='Wife') { echo 'selected'; } ?>>Wife</option>

<option value="Daughter" <?php if($row['relation']=='Daughter') { echo 'selected'; } ?>>Daughter</option>

<option value="Son" <?php if($row['relation']=='Son') { echo 'selected'; } ?>>Son</option>



</select>      </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" ></label>  
  <div class="col-md-4">
  
  <input name="Submit" type="submit"  value="Update" class="btn btn-rw btn-primary button1">

    
  </div>
</div>

</fieldset>
  </form>
 
  
  </div>
  
</div>

            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. designed by <a href="#">R.S Software</a></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="js/lib/morris-chart/raphael-min.js"></script>
    <script src="js/lib/morris-chart/morris.js"></script>
    <script src="js/lib/morris-chart/dashboard1-init.js"></script>


	<script src="js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.init.js"></script>

    <script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="js/lib/owl-carousel/owl.carousel-init.js"></script>

    <!-- scripit init-->

    <script src="js/scripts.js"></script>

</body>

</html>