<?php include('header.php'); ?>


        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Payments</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
           	
           
           
     <?php  
     $query = mysqli_query($conn,"SELECT plan_id FROM `stripe_payment` WHERE user_id = '".$_SESSION['user_id']."' GROUP BY plan_id" );
            while($plan_row = mysqli_fetch_array($query)) {
             //$plan_ids = array();
             //if(mysqli_num_rows($plan_row)>0){

               $plan_ids[] = $plan_row['plan_id'];
             
             

            }// end of while loop

          if(isset($plan_ids)){

            foreach ($plan_ids as $plan_id) {
              $plan_query = mysqli_query($conn,"SELECT * FROM `stripe_payment` WHERE user_id = '".$_SESSION['user_id']."' AND plan_id = $plan_id " );
              $total = mysqli_query($conn,"SELECT sum(price) as total FROM stripe_payment WHERE user_id='".$_SESSION['user_id']."' AND plan_id = $plan_id");
              $t = mysqli_fetch_assoc($total);
            

         // }
            
           // }//end of if
      ?>	


      <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active p-20" id="all" role="tabpanel">
                                        
                                      <div class="">
                                            <div class="">
                                                
                                                <div class="table-responsive m-t-40">
                                                    <table  class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th data-sortable="false">Sno</th>
                                                                <th data-sortable="false">Plan name</th>
                                                                <th data-sortable="false">Plan Type</th>
                                                                <th data-sortable="false">Plan Period</th>
                                                                <th>Price</th>
                                                                <th>Total Cycle</th> 
                                                                <th data-sortable="false">Payment Date</th>                         
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                            <?php
                            $i=1;
                            while($row = mysqli_fetch_assoc($plan_query))
                            {
                              
                           $plan = mysqli_query($conn,"SELECT * FROM `plan` WHERE id = '".$row['plan_id']."'");
                           $plan_row = mysqli_fetch_assoc($plan);
                            $main_plan_id = $plan_row['id_saving_plan'];
                            $main_plan = mysqli_query($conn,"SELECT * FROM `saving_plan` WHERE saving_plan_id = $main_plan_id");
                            $main_plan_row = mysqli_fetch_assoc($main_plan);
                            $type_id = $plan_row['type_id'];
                            $period_id = $plan_row['period_id'];
                           $type = mysqli_query($conn,"SELECT type FROM `type` WHERE id = $type_id");
                           $type_row = mysqli_fetch_assoc($type);
                            $period = mysqli_query($conn,"SELECT period FROM `period` WHERE id = $period_id");
                           $period_row = mysqli_fetch_assoc($period);

                                                          ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $main_plan_row['saving_plan_name']; ?></td>
                                                                <td><?php echo  $type_row['type']; ?></td>
                                                                <td><?php echo $period_row['period']; ?></td>
                                                                <td><?php echo $row['price']; ?></td>
                                                                <td><?php echo $row['total_cycle']; ?></td>
                                                                <td><?php echo $row['payment_date']; ?></td>
                                                               
                                                            </tr>
                                                           
                                                        <?php $i++;
                                                         } ?>    
                                                        </tbody>
                                                    </table>
                                                    <hr>
                                                    <?php if(isset($t)){ echo "Total Amount Paid : " .$t['total'];} ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>  
                                
                                
                            </div>

                          <?php } 
                             }
                             else{
                              echo "You have not made any payment yet";
                             }

                          ?>
                        </div>
                    </div> 

                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. designed by <a href="#">R.S Software</a></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="js/lib/morris-chart/raphael-min.js"></script>
    <script src="js/lib/morris-chart/morris.js"></script>
    <script src="js/lib/morris-chart/dashboard1-init.js"></script>


	<script src="js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.init.js"></script>

    <script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="js/lib/owl-carousel/owl.carousel-init.js"></script>

    <!-- scripit init-->

    <script src="js/scripts.js"></script>


     <script src="js/lib/datatables/datatables.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="js/lib/datatables/datatables-init.js"></script>

<script>
 $(document).ready(function(){
                $("#shw").click(function(){
                    $("#shwcont").toggle(500);
                     $("i", this).toggleClass("fa fa-caret-down fa fa-caret-up");
                });
  });
</script>

</body>

</html>