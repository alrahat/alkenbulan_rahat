<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<?php include('header.php'); ?>
<?php include('../Mahmud_stripe.php'); ?>

<style>
    /**
    * The CSS shown here will not be introduced in the Quickstart guide, but shows
    * how you can use CSS to style your Element's container.
    */
    .StripeElement {
        background-color: white;
        height: 40px;
        padding: 10px 12px;
        border-radius: 4px;
        border: 1px solid transparent;
        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
</style>

<?php
 
$settings = new Settings();
$stripe = new Mahmud_stripe();
$pk = $stripe->pk;
$user_card = $stripe->getUserCard($_SESSION['user_id']);
?>;

<?php
if (isset($_POST)) {
    if (!empty($_POST)) {

        if ($_POST['new_or_change_card'] == 'new_card') {
            $stripe->process_new_card_request();
            echo '<script type="text/javascript">
           window.location = "stripe_card.php";
          </script>'; 
        } else if ($_POST['new_or_change_card'] == 'change_card') {
            $stripe->change_card();
        }

    }
}


?>

<!-- Page wrapper  -->
<div class="page-wrapper"">
<!-- Bread crumb -->

<?php if (isset($_GET)) { ?>

    <?php if (isset($_GET['success_message'])) { ?>
        <div class="alert alert-success">
            <strong>Success! </strong> <?= $_GET['success_message'] ?>
        </div>
    <?php } ?>


    <?php if (isset($_GET['error_message'])) { ?>
        <div class="alert alert-warning">
            <strong>Warning!</strong> <?= $_GET['error_message'] ?>
        </div>
    <?php } ?>

<?php } ?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Card Details </h3></div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">

    <?php if ($user_card['existence']) { ?>
        <div class="text-center">
            <p>
                <b>Current Card: </b> <?= $user_card['default_source'];  ?>,<b> Last 4 digits: </b> <?= $user_card['last_4'] ?>
            </p>

         
            <p class="text-center">
                <button class="btn btn-success" name="change_card" id="change_card">Change Existing Card</button>
            </p>
            <br><br><br>
        </div>
    <?php } ?>
    <?php if (!$user_card['existence']) { ?>
    <div class="text-center">
        <p>
            <?= $user_card['message'] ?>
        </p>
        <p>
            <strong>
                Please Enter Your Debit / Credit Card Number
            </strong>

        </p>
        <?php } ?>
        <script src="https://js.stripe.com/v3/"></script>

        <form method="post" id="payment-form" <?php if($user_card['existence']){ ?> style="display: none;" <?php  }  ?>>
            <input type="hidden" name="new_or_change_card"
                   value="<?= $user_card['existence'] ? 'change_card' : 'new_card' ?>">
            <div class="form">
               <!--  <label for="card-element">
                    Credit or debit card
                </label> -->
                <div id="card-element">
                    <!-- A Stripe Element will be inserted here. -->
                </div>

                <!-- Used to display form errors. -->
                <div id="card-errors" role="alert"></div>
            </div>

            <button class="btn btn-block btn-success m-t-10">Submit Card Info</button>
        </form>

        <div id="pk-div" stripe-publishable-key="<?= $pk ?>">
        </div>

    </div>
</div>

</div>
<!-- End PAge Content -->
</div>
<!-- End Container fluid  -->
<!-- footer -->
<footer class="footer"> © 2018 All rights reserved. designed by <a href="#">R.S Software</a></footer>
<!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->
<!-- All Jquery -->
<script src="js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="js/lib/bootstrap/js/popper.min.js"></script>
<script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->


<!-- Amchart -->
<script src="js/lib/morris-chart/raphael-min.js"></script>
<script src="js/lib/morris-chart/morris.js"></script>
<script src="js/lib/morris-chart/dashboard1-init.js"></script>


<script src="js/lib/calendar-2/moment.latest.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/semantic.ui.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/prism.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
<!-- scripit init-->
<script src="js/lib/calendar-2/pignose.init.js"></script>

<script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="js/lib/owl-carousel/owl.carousel-init.js"></script>

<!-- scripit init-->

<script src="js/scripts.js"></script>




<script>
    var $pk_div = document.getElementById('pk-div');
    var pk = $pk_div.getAttribute('stripe-publishable-key');

    // Create a Stripe client.
    var stripe = Stripe(pk);

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            lineHeight: '18px',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();

        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });

    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        //alert(token.id);
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'token');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
        
        // Submit the form
        form.submit();
    }
</script>

<script type="text/javascript">
 $(document).ready(function(){    
 $("#change_card").click(function(){
    $("#payment-form").toggle();
  });



 });


</script>


</body>

</html>
