<?php include('header.php'); ?>

    <div class="page-wrapper">   
 <?php

if(isset($_GET['current']))
{
	echo '<div class="alert alert-warning">
  <strong>Current Password does not matched!</strong> 
</div>';
	
}
if(isset($_GET['confirm']))
{
	echo '<div class="alert alert-warning">
  <strong> New Password And Confirm Password not matched</strong> 
</div>';
	
}
if(isset($_GET['success']))
{
	echo '<div class="alert alert-success">
  <strong>Updated Successfully</strong> 
</div>';
	
}
 ?>
<div class="container">
  <div class="col-md-10 " style="margin-top:60px; margin-bottom:60px;">
   <form class="form-horizontal" method="POST" action="../changepassword.php?id=<?php echo $_SESSION['user_id'];?>" >
    <fieldset>

<!-- Form Name -->
<legend>Change Password</legend>

  <div class="form-group">
  <label class="col-md-4 control-label" for="Name (Full name)">Old Password</label>  
  <div class="col-md-4">
 <div class="input-group">
       <div class="input-group-addon">
        <i class="fa fa-lock">
        </i>
       </div>
       <input  name="oldpass" type="Password" placeholder="Old Password"  class="form-control input-md" required="required">
      </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Name (Full name)">New Password</label>  
  <div class="col-md-4">
 <div class="input-group">
       <div class="input-group-addon">
        <i class="fa fa-lock">
        </i>
       </div>
       <input  name="newpass" type="Password" placeholder="New Password"  class="form-control input-md" required="required">
      </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Name (Full name)">Confirm New Password</label>  
  <div class="col-md-4">
 <div class="input-group">
       <div class="input-group-addon">
        <i class="fa fa-lock">
        </i>
       </div>
       <input  name="cnewpass" type="Password" placeholder="Confirm New Password"  class="form-control input-md" required="required">
      </div>
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" ></label>  
  <div class="col-md-4">
  
  <input name="Submit" type="submit"  value="Change" class="btn btn-rw btn-primary button1">

    
  </div>
</div>

</fieldset>
  </form>
  </div>
</div>
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. designed by <a href="#">R.S Software</a></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="js/lib/morris-chart/raphael-min.js"></script>
    <script src="js/lib/morris-chart/morris.js"></script>
    <script src="js/lib/morris-chart/dashboard1-init.js"></script>


	<script src="js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.init.js"></script>

    <script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="js/lib/owl-carousel/owl.carousel-init.js"></script>

    <!-- scripit init-->

    <script src="js/scripts.js"></script>

</body>

</html>