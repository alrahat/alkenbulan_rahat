<?php
session_start();
if(!$_SESSION['user_id'])
{
	header("location:../index.php");
}

  include('../config.php'); 
  $full_name = $_SERVER['PHP_SELF'];
       $name_array = explode('/',$full_name);
        $count = count($name_array);
      $page_name = $name_array[$count-1];
  ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/dokan-sadara-logo.webp">
    <title>Savings Partners</title>
    <!-- Bootstrap Core CSS -->
    <link href="css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->

    <link href="css/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
    <link href="css/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
    <link href="css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="css/helper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesnt work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">
                        <!-- Logo icon -->
                        <b><img src="../images/logos/1.png" alt="homepage" class="dark-logo" style="width:45px; height:45px;"/></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <!-- <span><img src="images/logo-text.png" alt="homepage" class="dark-logo" /></span> -->
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                       
                        
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="images/users/5.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="profile.php"><i class="ti-settings"></i> Profile</a></li>
                                      <li><a href="account_info.php"><i class="ti-wallet"></i> Withdraw Balance</a></li>
                                    <li><a href="account_settings.php"><i class="ti-arrows-horizontal"></i> Change Password</a></li>
                                    
                                    <li><a href="../logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="<?php if($page_name=='index.php' ){?>active<?php }?>"><a href="index.php"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </a></li>

                        <li > <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-credit-card"></i><span class="hide-menu">Payment Method</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li ><a href="stripe_card.php" >Credit / Debit Card</a></li>

                            </ul>
                        </li>
                        <li > <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">Individual Savings Plan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li ><a href="indivitual.php" >View plans</a></li>
                
                                
                            </ul>
                        </li>
					  <li > <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Family  Savings Plan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li ><a href="join.php" >View Joined plans</a></li>
                                <li ><a href="created.php" >View Created plans</a></li>
                                <li ><a href="group_members.php" >View Group Members</a></li>
							<!-- 	<li ><a href="order_group_members.php" >Order Group Members</a></li> -->
                                
                            </ul>
                        </li>

                        <li > <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-money"></i><span class="hide-menu">Payments</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li ><a href="user_payment.php" >View Payments</a></li>
                                <li ><a href="late_payment.php" >Late Payment</a></li>
                
                                
                            </ul>
                        </li>
                        <li > <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-handshake-o"></i><span class="hide-menu">Request to join Plan</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="requested_plan.php" >View Plan</a></li>
                                
                            </ul>
                        </li>
                          <li><a href="feedback.php"><i class="fa fa-comment"></i><span class="hide-menu">Feedback</a></li>

                          <li><a href="../index.php"><i class="fa fa-home"></i><span class="hide-menu">Back to Home</a></li>
                          
                          
                        <li>
                            
                            <a href="../logout.php"><i class="fa fa-power-off"></i><span class="hide-menu">Logout</a></li>

                       
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
