<?php include('header.php'); ?>


        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Payments</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
            
           
           
     <?php  
     $query = mysqli_query($conn,"SELECT group_id FROM `group_members` WHERE group_member_id = '".$_SESSION['user_id']."' GROUP BY group_id" );
            while($group_row = mysqli_fetch_array($query)) {
             //$plan_ids = array();
             //if(mysqli_num_rows($plan_row)>0){

               $group_ids[] = $group_row['group_id'];
             
             

            }// end of while loop

          if(isset($group_ids)){

            foreach ($group_ids as $group_id) {
              $group_query = mysqli_query($conn,"SELECT group_members.group_member_id,group_members.is_joined,user_selected_group_plan.user_id,user_selected_group_plan.plan_type,user_selected_group_plan.plan_period,user_selected_group_plan.plan_amount,user_selected_group_plan.groupName,type.id as type_id,type.type,period.id as period_id,period.period FROM `group_members` INNER JOIN user_selected_group_plan ON user_selected_group_plan.id = group_members.group_id INNER JOIN type ON type.id = user_selected_group_plan.plan_type INNER JOIN period ON period.id = user_selected_group_plan.plan_period WHERE group_id = $group_id ");
            
      ?>    


      <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active p-20" id="all" role="tabpanel">
                                        
                                      <div class="">
                                            <div class="">
                                                
                                                <div class="table-responsive m-t-40">
                                                    <table  class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th data-sortable="false">Sno</th>
                                                                <th data-sortable="false">Group Name</th>
                                                                <th data-sortable="false">User Name</th>
                                                                <th data-sortable="false">Plan Type</th>
                                                                <th>Plan Period</th>
                                                                <th>Price</th> 
                                                                <th data-sortable="false">Status</th>                         
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                            <?php
                            $i=1;
                            while($row = mysqli_fetch_assoc($group_query))
                            {
                          
                            $get_member_info = mysqli_query($conn,"SELECT * FROM register_user WHERE id = '".$row['group_member_id']."'");
                            $member_row = mysqli_fetch_assoc($get_member_info);


                                                          ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                 <td><?php echo $row['groupName']; ?></td>
                                                                <td><?php echo $member_row['fname']; ?>  <?php echo $member_row['lname']; ?></td>
                                                                <td><?php echo $row['type']; ?></td>
                                                                <td><?php echo $row['period']; ?></td>
                                                                <td><?php echo $row['plan_amount']; ?></td>
                                                                <td><?php if($row['is_joined'] == 1){ echo "Joined"; } else { echo "Pending";}?></td>
                                                               
                                                            </tr>
                                                           
                                                        <?php $i++;
                                                         } ?>    
                                                        </tbody>
                                                    </table>
                                                    <hr>
                                                    <?php if(isset($t)){ echo "Total Amount Paid : " .$t['total'];} ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>  
                                
                                
                            </div>

                          <?php } 
                             }
                             else{
                              echo "You have not made any payment yet";
                             }

                          ?>
                        </div>
                    </div> 

                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. designed by <a href="#">R.S Software</a></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="js/lib/morris-chart/raphael-min.js"></script>
    <script src="js/lib/morris-chart/morris.js"></script>
    <script src="js/lib/morris-chart/dashboard1-init.js"></script>


    <script src="js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.init.js"></script>

    <script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="js/lib/owl-carousel/owl.carousel-init.js"></script>

    <!-- scripit init-->

    <script src="js/scripts.js"></script>


     <script src="js/lib/datatables/datatables.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="js/lib/datatables/datatables-init.js"></script>

<script>
 $(document).ready(function(){
                $("#shw").click(function(){
                    $("#shwcont").toggle(500);
                     $("i", this).toggleClass("fa fa-caret-down fa fa-caret-up");
                });
  });
</script>

</body>

</html>