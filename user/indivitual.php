<?php include('header.php'); ?>


        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Individual Plans</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
           	
           
     <?php  $query = mysqli_query($conn,"SELECT * FROM `user_selected_plan` WHERE user_id='".$_SESSION['user_id']."'");

      
?>	  
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active p-20" id="all" role="tabpanel">
                                        
                                      <div class="">
                                            <div class="">
                                                
                                                <div class="table-responsive m-t-40">
                                                    <table id="myTableall" class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th data-sortable="false">Sno</th>
                                                                <th data-sortable="false">Created on</th>
                                                                <th data-sortable="false">Plan name</th>
																 <th data-sortable="false">Plan Type</th>

                                                                <th data-sortable="false">Plan Period</th>
														      <th>Price</th>
                                                                
                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
														<?php
														$i=1;
														while($row = mysqli_fetch_assoc($query))
														{
															
 $query1 = mysqli_query($conn,"SELECT * FROM `type` WHERE id='".$row['plan_type']."'");
$row1 = mysqli_fetch_assoc($query1);
 $query2 = mysqli_query($conn,"SELECT * FROM `period` WHERE id='".$row['plan_period']."'");
$row2 = mysqli_fetch_assoc($query2);
															?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $row['created_on']; ?></td>
                                                                <td><?php echo 'Individual Savings Plan'; ?></td>
                                                                <td><?php echo $row1['type']; ?></td>
                                                                <td><?php echo $row2['period']; ?></td>
                                                                <td><?php echo $row['plan_amount']; ?></td>
                                                               
                                                            </tr>
                                                           
                                                        <?php $i++;
														} ?>    
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div> 

                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. designed by <a href="#">R.S Software</a></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="js/lib/morris-chart/raphael-min.js"></script>
    <script src="js/lib/morris-chart/morris.js"></script>
    <script src="js/lib/morris-chart/dashboard1-init.js"></script>


	<script src="js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.init.js"></script>

    <script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="js/lib/owl-carousel/owl.carousel-init.js"></script>

    <!-- scripit init-->

    <script src="js/scripts.js"></script>


     <script src="js/lib/datatables/datatables.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="js/lib/datatables/datatables-init.js"></script>

<script>
 $(document).ready(function(){
                $("#shw").click(function(){
                    $("#shwcont").toggle(500);
                     $("i", this).toggleClass("fa fa-caret-down fa fa-caret-up");
                });
  });
</script>

</body>

</html>