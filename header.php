<?php

   session_start(); //4.3.19

// Turn off all error reporting
error_reporting(0);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Denis Samardjiev" />
	<meta name="description" content="Raleway | Mega Bootstrap Template">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<title>Saving Partners</title>
	
	<!-- Royal Preloader CSS -->
	<link rel="stylesheet" type="text/css" href="css/royal_preloader.css">

	<!-- jQuery Files -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	<!-- Royal Preloader -->
	<script type="text/javascript" src="js/royal_preloader.min.js"></script>

	<!-- Revolution Slider CSS -->
	<link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
	<link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
		
	<!-- Revolution Slider Navigation CSS -->
	<link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">

	<!-- Stylesheets -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" title="main-css">
	<link href="css/bootstrap-social.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet" >
	<link href="css/jquery.snippet.css" rel="stylesheet">
	<link href="css/buttons.css" rel="stylesheet">

	<!-- Style Switcher / remove for production -->
	<link href="css/style-switcher.css" rel="stylesheet">
	
	<!-- Alternate Stylesheets / choose what color you want and include regularly AFTER style.css above -->
	<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/green.css" title="green">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/purple.css" title="purple">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/dark-blue.css" title="dark-blue">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/red.css" title="red">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/silver.css" title="silver">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/pinkish.css" title="pinkish">
	<link rel="alternate stylesheet" type="text/css" href="css/colors/seagul.css" title="seagul">
	<link rel="alternate stylesheet" type="text/css" href="css/width-full.css" title="width-full">
	<link rel="alternate stylesheet" type="text/css" href="css/width-boxed.css" title="width-boxed">

	<!-- Icon Fonts -->
	<link href='css/ionicons.min.css' rel='stylesheet' type='text/css'>
	<link href='css/font-awesome.css' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Magnific Popup -->
	<link href='css/magnific-popup.css' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	
</head>
<body class="royal_preloader scrollreveal">

	<div id="royal_preloader"></div>
	
	

	<!-- Begin Boxed or Fullwidth Layout -->
	<div id="bg-boxed">
	    <div class="boxed">

			<!-- Begin Header -->
			<header>

				<!-- Begin Top Bar -->
				<div class="top-bar">
					<div class="container">
						<div class="row">
							<!-- Address and Phone -->
							<div class="col-sm-7 hidden-xs">
								<span class="ion-android-system-home home-icon" style="color: #00b7eb;"></span>24 Page Avenue
Wembley, Middlesex HA9 9FP, UK<span class="ion-ios7-telephone phone-icon" style="color: #00b7eb;"></span>(+44) 7533695264
							</div>
							<!-- Social Buttons -->
							<div class="col-sm-5 text-right">
				                <ul class="topbar-list list-inline">
					                <li>						
							            <a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-google-plus"></i>
										</a>
							            <a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-twitter"></i>
										</a>
										<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-instagram"></i>
										</a>
										<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-facebook"></i>
										</a>
									</li>
								<?php
									if($_SESSION['user_id']=="")
									{
								?>
									<li><a href="login.php">Login</a></li>
									<li><a href="register.php">Register</a></li>
								<?php
									}else{
									?>
								<li>
								
								
								
								
								  <div class="dropdown">
									  <a class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $_SESSION['username'] ;?>
									  <span class="caret"></span></a>
									  <ul class="dropdown-menu">
									    <li><a href="user/index.php" style="color:#000;">Dashboard<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
									    <li><a href="logout.php" style="color:#000;">Sign Out <span class="glyphicon glyphicon-log-out pull-right"></span></a></li>
									  </ul>
							      </div> 
								
								           
									       
								
								
								
								
								</li>	
								<li><a href="logout.php">Logout</a></li>
								<?php	
									} 
								?>
								</ul>
							</div>
						</div><!--/row --> 
					</div><!--/container header -->   
				</div><!--/top bar -->   
				<!-- End Top Bar -->

				<!-- Login -->
				<!-- <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
				    <div class="modal-dialog modal-sm">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				                <h4 class="modal-title" id="loginLabel">Login</h4>
				            </div>
				            <div class="modal-body">
					            <form role="form">
						            <div class="form-group">
						                <label for="exampleInputEmail1">Email address</label>
						                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						            </div>
						            <div class="form-group">
						                <label for="exampleInputPassword1">Password</label>
						                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
						            </div>
						            <div class="checkbox">
						                <label>
						                    <input type="checkbox"> Remember Me
						                </label>
						            </div>
						        </form>
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-rw btn-primary">Login</button>
				            </div>
				        </div>
				    </div>
				</div> -->
				<!-- End Login -->

		     	<!-- Begin Navigation -->
		     	<div class="navbar-wrapper">
					<div class="navbar navbar-main" id="fixed-navbar">
						<div class="container">
							<div class="row">
								<div class="col-sm-12 column-header">
									<div class="navbar-header">
										<!-- Brand -->
										<a href="index.php" class="navbar-brand">
										<!--   <img src="images/logos/finalolgo1.png"  >  -->
										 
										<!-- <img src="images/logos/logoasc_20180804_121458958.jpg" style="    height: 80px;
    width: 200px;
    margin-top: -25px;" >   -->
   <!--  <img src="images/logos/logo1.png" style="    height: 80px;
    width: 200px;
    margin-top: -25px;" >  -->
    <img src="images/logos/1.png" style="    height: 60px;
    width: 200px; margin-top: -15px;" >
											<!--<h2 style=" color:#00b7eb;">Saving Partners</h2>  -->
											<!-- <h2 style=" color:#00b7eb; margin-left: 175px;">asc</h2>  -->
										</a>
										<!-- Mobile Navigation -->
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
								            <span class="sr-only">Toggle navigation</span>
								            <span class="icon-bar"></span>
								            <span class="icon-bar"></span>
								            <span class="icon-bar"></span>
										</button>
									</div><!-- /navbar header -->   

									<!-- Main Navigation - Explained in Documentation -->
									<nav class="navbar-collapse collapse navHeaderCollapse" role="navigation">
										<ul class="nav navbar-nav navbar-right">
											
									        <li >
									        	<a href="index.php" >Home</a>
									        	
									        </li>
									        <li >
									        	<a href="about.php" >About Us</a>
									        	
									        </li>
									        <li >
									        	<a href="services.php" >Services</a>
									        	
									        </li>
									        <li >
									        	<a href="membership.php" >Membership</a>
									        	
									        </li>
									        <li >
									        	<a href="joining-process.php" >Joining Process</a>
									        	
									        </li>
									       <!-- <li >
									        	<a href="#" >Testimonial</a>
									        	
									        </li>  -->
									        
									        <?php
									if($_SESSION['user_id']!="")
									{
								?>
									         <li >
									        	<a href="select_plans.php" >Select A Plan</a>
									        	
									        </li> 
									        
									        <?php
									        
									        }
									        
									        
									        ?>
									          <!--<li >
									        	<a href="blog.php" >Blog</a>
									        	
									        </li>-->
									         <li >
									        	<a href="contact.php" >Contact</a>
									        	
									        </li>
									        
											<!--<li class="dropdown dropdown-main">
									        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog<span class="fa fa-angle-down dropdown-arrow"></span></a>
									        	<ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
									            	<li><a href="blog-posts-sidebar-right.php">Sidebar Right</a></li>
									            	<li><a href="blog-posts-sidebar-left.php">Sidebar Left</a></li>
									            	<li><a href="blog-posts-fullwidth.php">Full Width</a></li>
									            	<li><a href="blog-posts-grid.php">Grid</a></li>
									            	<li><a href="blog-single-post.php">Single Post</a></li>
									        	</ul>
									        </li>-->
											
										</ul><!-- /navbar right --> 
									</nav><!-- /nav -->
								</div>
							</div>
						</div><!-- /container header -->   
					</div><!-- /navbar -->
				</div><!-- /navbar wrapper -->
				<!-- End Navigation -->

			</header><!-- /header -->
			<!-- End Header -->
			 <?php
			 $log='';
			 //$log=$_GET['log'];
      if($_GET['log'])
      {
      ?>
      <div class="alert alert-success" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> You have been signed in successfully!
</div>
      <?php   
      }
    
    ?>
    
     <?php
      if($_GET['msg1'])
      {
      ?>
      <div class="alert alert-success" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> You have been logout successfully!
</div>
      <?php   
      }
    
    ?>
      <?php
      if($_GET['error'])
      {
      ?>
      <div class="alert alert-danger" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> 'Email Already Existed'!  <br>
  <p></p>
</div>
      <?php   
      }
    
    ?>
    <?php
      if($_GET['er4'])
      {
      ?>
      <div class="alert alert-danger" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> 'Incorrect email and password combination’'!
</div>
      <?php   
      }
    
    ?>
    
    
     <?php
      if($_GET['con_message'])
      {
      ?>
      <div class="alert alert-success" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> Your Message send successfully!
</div>
      <?php   
      }
    
    ?>
    
    
    
     <?php
      if($_GET['successin'])
      {
      ?>
      <div class="alert alert-success" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> Payment successfully done! Please check your payment details on dashboard.
</div>
      <?php   
      }
    
    ?>

     <?php
      if($_GET['group_success'])
      {
      ?>
      <div class="alert alert-success" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> Group successfully created and email sent to added members !
</div>
      <?php   
      }
    
    ?>

     <?php
      if($_GET['added_in_group'])
      {
      ?>
      <div class="alert alert-success" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> You have successfully joined in the group !
</div>
      <?php   
      }
    
    ?>
    
    
     <?php
      if($_GET['passfail'])
      {
      ?>
      <div class="alert alert-danger" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error!</strong> Password failed to send Try again !
</div>
      <?php   
      }
    
    ?>
    
     <?php
      if($_GET['wrongid'])
      {
      ?>
      <div class="alert alert-danger" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Please provide correct email id  !
</div>
      <?php   
      }
    
    ?>
     <?php
      if($_GET['sendpass'])
      {
      ?>
      <div class="alert alert-info" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> Password has been send to your email  !
</div>
      <?php   
      }
    
    ?>
    
      <?php
      if($_GET['msg'])
      {
      ?>
      <div class="alert alert-success" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> Your Message send successfully!
</div>
      <?php   
      }
    
    ?>

    <!---Rahat-->

    <?php
      if($_GET['pay_fail'])
      {
      ?>
      <div class="alert alert-danger" role="alert" style="position:fixed; right:0px;z-index: 999999;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Payment Failed!
</div>
      <?php   
      }
    
    ?>

     

    <!--Rahat-->
    <!-- Main Heder End -->
    
    <style>
    .address {
    color: #ffffff;
    font-weight: 400;
    margin-top: 20px;
}
    </style>
    