<?php
include_once('Mahmud_email.php');
include_once('Mahmud_utility.php');

function send_inv_mails($user_ids,$group_creator_id)
{
   
    $msg = array();
    
    //sending email to selected members
    foreach ($user_ids as $user_id) {

        $mail_sent = send_email_to_user($user_id,$group_creator_id);

        $msg[] = $mail_sent['message'];
    }
    return $msg;
}

function send_email_to_user($user_id,$group_creator_id)
{
    $me = new Mahmud_email();
    $mu = new Mahmud_utility();

    $user = $mu->getUser($user_id);
    $group_creator = $mu->getUser($group_creator_id);

    
    $email_to = $user['email'];


    $mail_data['to'] = $email_to;
    $mail_data['subject'] = "You are invited";
    $mail_data['message'] = "You are invited to join a 'Family and Friends plan' created by {$group_creator['fname']} {$group_creator['lname']}.Please go to your account and check for details";


    return $me->send($mail_data);

}

//Email for stripe failed payment

function send_payment_mails($user_ids)
{
    
    $msg = array();
    //sending email to selected members
    foreach ($user_ids as $user_id) {

        $mail_sent = send_payment_email_to_user($user_id);

        $msg[] = $mail_sent['message'];
    }
    return $msg;
}

function send_payment_email_to_user($user_id)
{
    $me = new Mahmud_email();
    $mu = new Mahmud_utility();

    $user = $mu->getUser($user_id);
    $email_to = $user['email'];


    $mail_data['to'] = $email_to;
    $mail_data['subject'] = "Payment Failed";
    $mail_data['message'] = "Payment failed.We could not receive payment for your plan.Please go to your dashboard and click on 'late payment' from menubar";

    return $me->send($mail_data);

}

//Email for stripe failed payment

    ?>