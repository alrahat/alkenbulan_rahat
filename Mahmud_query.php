<?php

include_once('config.php');
include_once('Mahmud_query.php');

Class Mahmud_query
{

    public $connection;


    public function __construct($connection = false)
    {
        global $conn;

        if (!$connection) {
            $connection = $conn;

            //var_dump($conn);
            //var_dump($connection);exit;
        }



        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }

        $this->connection = $connection;
    }

    public function query($sql)
    {
        if ($executed_query = mysqli_query($this->connection, $sql)) {

        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($this->connection);
            die();
        }

        return $executed_query;
    }

    public function row($sql)
    {
        $executed_query = $this->query($sql);

        $row = $executed_query->fetch_assoc();

        mysqli_free_result($executed_query);

        return $row;
    }

    public function rows($sql)
    {
        $executed_query = $this->query($sql);

        $rows = array();
        while($row = $executed_query->fetch_assoc()){
            $rows[] = $row;
        }

        mysqli_free_result($executed_query);

        return $rows;
    }

    public function last_query()
    {
        return mysqli_info($this->connection);
    }

    public function last_id()
    {
        $last_id = $this->connection->insert_id;
        return $last_id;
    }

    public function insert($table, $data)
    {
        $ret = false;
        $sql = $this->insert_string($table, $data);

        $executed_query = $this->query($sql);

    }

    public function update($table, $data, $where_string)
    {
        $ret = false;
        $sql = $this->update_string($table, $data, $where_string);

        $executed_query = $this->query($sql);

    }

    public function insert_string($table, $data)
    {
        $fields = $values = array();

        foreach ($data as $key => $val) {
            $fields[] = $key;
            $values[] = "'$val'";
        }

        return $this->_insert($table, $fields, $values);
    }

    protected function _insert($table, $keys, $values)
    {
        return 'INSERT INTO ' . $table . ' (' . implode(', ', $keys) . ') VALUES (' . implode(', ', $values) . ')';
    }


    protected function _update($table, $values)
    {
        foreach ($values as $key => $val) {
            $valstr[] = $key . ' = ' . "'$val'";
        }

        return 'UPDATE ' . $table . ' SET ' . implode(', ', $valstr);
    }

    public function update_string($table, $data, $where_string)
    {
        if (empty($where_string)) {
            return FALSE;
        }


        $fields = array();
        foreach ($data as $key => $val) {
            $fields[$key] = $val;
        }

        $sql = $this->_update($table, $fields) ;
        $sql .=  " {$where_string} ";

        return $sql;
    }


}


?>