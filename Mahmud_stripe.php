<?php

include_once('stripe-php-master/init.php');
include_once('Mahmud_query.php');
include_once('Settings.php');
include_once('Mahmud_utility.php');
include_once('Mahmud_email.php');
include_once('send_inv_mails.php');
//include_once('send_payment_mails.php')




Class Mahmud_stripe
{   //This is a custom class to handle stripe
    public $mq;
    public $settings;
    public $utility;
    public $sk;
    public $pk;



    public function __construct()
    {

        $this->mq = new Mahmud_query();
        $this->settings = new Settings();
        $this->utility = new Mahmud_utility();
        $this->sk = $this->settings->get_stripe_secret_key();
        $this->pk = $this->settings->get_stripe_public_key();
        //$this->conn = $conn;
    }



    public function process_new_card_request()
    {
        $user_id = $_SESSION['user_id'];
        $user = $this->utility->getUser($user_id);

       


        if (empty($user)) {
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=No user is found!");
        }

        if (!isset($_POST)) {
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=nothing is posted!");
        }

        $token = $_POST['token'];
        $email = $user['email'];

        if ($token == "") {
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=token is not given!");
        }

        $customer = null;
        $stripe_customer = $this->create_stripe_customer($email, $token);

        $customer = $stripe_customer['customer'];
        if (empty($customer) || !empty($stripe_customer['errors'])) {

            $message = "Could not create customer";
            if (!empty($stripe_customer['errors'])) {
                $message = implode(" | ", $stripe_customer['errors']);
            }

            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message={$message}");
        }

        //----------------------------------------------------------------------
        $customer_exist = $this->utility->stripe_customer_exists($user_id);

        if (!$customer_exist) {
            $set_customer = $this->set_stripe_customer($customer->id, $user_id);

            if (!$set_customer) {
                $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=Could not set customer");
            }
        }
        //----------------------------------------------------------------------
    }

    public function create_stripe_customer($email, $token)
    {


        $sk = $this->sk;
        $errors = array();
        $stripe_customer = array();
        $customer = null;

        try {
            \Stripe\Stripe::setApiKey($sk);
            $customer = \Stripe\Customer::create([
                "email" => $email,
                "source" => $token
            ]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        $stripe_customer['customer'] = $customer;
        $stripe_customer['errors'] = $errors;
        return $stripe_customer;
    }

    public function set_stripe_customer($stripe_customer_id, $user_id)
    {
        $user_stripe_customer = $this->utility->user_stripe_customer($user_id);
        $set_customer = false;
        if (empty($user_stripe_customer)) {
            $ins_data = array();
            $ins_data['user_id'] = $user_id;
            $ins_data['stripe_customer_id'] = $stripe_customer_id;

            $this->mq->insert('stripe_customer', $ins_data);
            $set_customer = true;
        }
        if (!empty($user_stripe_customer)) {
            if (empty($user_stripe_customer['stripe_customer_id'])) {
                $upd_data = array();
                $upd_data['stripe_customer_id'] = $stripe_customer_id;

                $this->mq->update('stripe_customer', $upd_data, " WHERE `user_id`={$user_id} ");
                $set_customer = true;
            }
        }
        

        return $set_customer;


    }

    public function getUserCard($user_id)
    {

        $card = array();
        $card['existence'] = false; 
        $card['message'] = "";
        $card['default_source'] = "";
        $card['last_4'] = "";

        $user_stripe_customer = $this->utility->user_stripe_customer($user_id);
        

        if (empty($user_stripe_customer)) {
            //$card['message'] = "Please enter your debit/credit card number";
            return $card;
        }


        $customer_id = $user_stripe_customer['stripe_customer_id'];

        /*---------------------------------------------------------------------*/

        $customer = null;

        $stripe_customer = $this->getCustomer($customer_id);

        $customer = $stripe_customer['customer'];
        if (!$customer || !empty($stripe_customer['errors'])) {

            $message = "Could not fetch customer";
            if (!empty($stripe_customer['errors'])) {
                $message = implode(" | ", $stripe_customer['errors']);
            }

            $card['message'] = $message;
            return $card;

        }

        $card['existence'] = true;
        $card['message'] = "Card found";
        $card['default_source'] = $customer->default_source->brand;
        $card['last_4'] = $customer->default_source->last4;

        return $card;
    }

    public function getCustomer($customer_id)
    {   

        $sk = $this->sk;
        $errors = array();
        $stripe_customer = array();
        $customer = null;

       
        try {
            \Stripe\Stripe::setApiKey($sk);
            $customer = \Stripe\Customer::Retrieve(
                ["id" => $customer_id, "expand" => ["default_source"]]
            );
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        $stripe_customer['customer'] = $customer;
        $stripe_customer['errors'] = $errors;
        

        return $stripe_customer;
       

    }

    public function change_card()
    {   


        $user_id = $_SESSION['user_id'];

        if (!isset($_POST)) {
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=noting is posted!");
        }

        $token = $_POST['token'];

        if ($token == "") {
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=token is not given");
        }

        $user_stripe_customer = $this->utility->user_stripe_customer($user_id);

        if (empty($user_stripe_customer)) {
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=Customer is not stored");
        }

        $customer_id = $user_stripe_customer['stripe_customer_id'];

        /*---------------------------------------------------------------------*/

        $customer = null;

        $stripe_customer = $this->getCustomer($customer_id);

        $customer = $stripe_customer['customer'];
        if (!$customer || !empty($stripe_customer['errors'])) {

            $message = "Could not fetch customer";
            if (!empty($stripe_customer['errors'])) {
                $message = implode(" | ", $stripe_customer['errors']);
            }
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message={$message}");
        }

        $errors = array();

        try {

            $customer->source = $token; // obtained with Checkout
            $customer->save();
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        if (!empty($errors)) {
            $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?error_message=" . implode(" | ", $errors));
        }

        $this->utility->js_redirect($this->utility->current_link($without_params = true) . "?success_message=Card changed successfully");

    }

    //testing

    public function charge_from_card($user_id,$group_id,$plan_id,$price,$cycle){

    
        $sk = $this->sk;
        //$conn = $this->conn;
        
        $x = $this->mq->row("SELECT stripe_customer_id FROM stripe_customer WHERE user_id = $user_id");
        $customer_id = ($x['stripe_customer_id']);
        //die;
       

        \Stripe\Stripe::setApiKey($sk);
        // Get the credit card details submitted by the form

        // Create a charge: this will charge the user's card
        $charge = null;
        $jsonerror = null;

        //$custome_id = ($conn,"SELECT * FROM stripe_customer");

        try {
          $charge = \Stripe\Charge::create(array(
                    "customer" => $customer_id,
                    "amount" => $price*100, // Amount in cents
                    "currency" => "usd",
                   // "source" => $token,
                    "description" => "Example charge",
                    
                  ));

          // $store_payment = $this->mq->row("INSERT INTO stripe_payment (user_id,plan_id,price,payment_date,total_cycle) VALUES (1,1,100,'2019-02-25',3)");
        } 
        catch (\Stripe\Error\Card $e) {
                // The card has been declined
                // Since it's a decline, \Stripe\Error\Card will be caught
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $jsonerror = json_encode($jsonerror);
          print('Status is:' . $e->getHttpStatus() . "\n");
          print('Type is:' . $err['type'] . "\n");
          print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            //print('Param is:' . $err['param'] . "\n");
          print('Message is:' . $err['message'] . "\n");
            // alert()->warning('Warning', 'Donation Failed.');
        }
        catch (\Stripe\Error\RateLimit $e) {
      // Too many requests made to the API too quickly
        } catch (\Stripe\Error\InvalidRequest $e) {
      // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Error\Authentication $e) {
      // Authentication with Stripe's API failed
      // (maybe you changed API keys recently)
        } catch (\Stripe\Error\ApiConnection $e) {
      // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
      // Display a very generic error to the user, and maybe send
      // yourself an email
        } catch (Exception $e) {
      // Something else happened, completely unrelated to Stripe
        }
          //Create the donation
        if ($charge) {

        $payment_date = date("Y-m-d");
        
        $store_payment = $this->mq->query("INSERT INTO stripe_payment (user_id,group_id,plan_id,price,total_cycle) VALUES ($user_id,$group_id,$plan_id,$price,$cycle)");
          
           $success = 'Payment Successful';
           echo $success;
        }
        
            $check_stripe_log = $this->mq->query("SELECT * FROM stripe_payment_log WHERE user_id = $user_id AND plan_id = $plan_id LIMIT 1");
            $log_row = mysqli_fetch_array($check_stripe_log);
            $counter = $log_row['counter'];
            $counter = $counter+1;
            $log_no = mysqli_num_rows($check_stripe_log);
            if($log_no == 1 && $counter!=3){
            
            $update_stripe_log = $this->mq->query("UPDATE stripe_payment_log SET counter = $counter WHERE user_id = $user_id AND plan_id = $plan_id");
           

            }
            
            else{
              $stripe_log = $this->mq->query("INSERT INTO stripe_payment_log (user_id,plan_id,counter) VALUES ($user_id,$plan_id,1)");
            }
            if($counter == 3){
                
               $msg = send_payment_mails([$user_id]);
                
            }

    }


    //testing


    public function start_charge_from_card($user_id,$price,$plan_id,$group_id,$cycle){


        $response = array();
       
        $sk = $this->sk;

        
        $x = $this->mq->row("SELECT stripe_customer_id FROM stripe_customer WHERE user_id = $user_id");
        $customer_id = ($x['stripe_customer_id']);
       

        \Stripe\Stripe::setApiKey($sk);
        // Get the credit card details submitted by the form

        // Create a charge: this will charge the user's card
        $charge = null;
        $jsonerror = null;

        //$custome_id = ($conn,"SELECT * FROM stripe_customer");

        try {
            $charge = \Stripe\Charge::create(array(
                    "customer" => $customer_id,
                    "amount" => $price*100, // Amount in cents
                    "currency" => "usd",
                   // "source" => $token,
                    "description" => "Example charge",
                    
                  ));
            $response['status'] = 'Success';
          // $store_payment = $this->mq->row("INSERT INTO stripe_payment (user_id,plan_id,price,payment_date,total_cycle) VALUES (1,1,100,'2019-02-25',3)");
        } 
        catch (\Stripe\Error\Card $e) {
                // The card has been declined
                // Since it's a decline, \Stripe\Error\Card will be caught
          $body = $e->getJsonBody();
          $err  = $body['error'];
          $jsonerror = json_encode($jsonerror);
          print('Status is:' . $e->getHttpStatus() . "\n");
          print('Type is:' . $err['type'] . "\n");
          print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            //print('Param is:' . $err['param'] . "\n");
          print('Message is:' . $err['message'] . "\n");
          $response['status'] = 'Error';
            // alert()->warning('Warning', 'Donation Failed.');
        }
        catch (\Stripe\Error\RateLimit $e) {
      // Too many requests made to the API too quickly
        } catch (\Stripe\Error\InvalidRequest $e) {
      // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Error\Authentication $e) {
      // Authentication with Stripe's API failed
      // (maybe you changed API keys recently)
        } catch (\Stripe\Error\ApiConnection $e) {
      // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
      // Display a very generic error to the user, and maybe send
      // yourself an email
        } catch (Exception $e) {
      // Something else happened, completely unrelated to Stripe
        }

        echo $e;
        


          //Create the donation
        if ($charge) {

        $payment_date = date("Y-m-d");
        
        $store_payment = $this->mq->query("INSERT INTO stripe_payment (user_id,group_id,plan_id,price,total_cycle) VALUES ($user_id,$group_id,$plan_id,$price,$cycle)");
       
        }
       
        return $response;

    }
    
}