<?php  
   include('header.php');
?>

			<!-- Begin Content Section -->
			<!-- /content -->
			<!-- End Content Section -->
    <div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 no-padding" style="margin-bottom: -7px;">
						<iframe style="border: 0px none; border-color:#fff; width:100%;" height="350" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2480.300047788343!2d-0.2717358846332207!3d51.56273277964425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761170c7f7fbd7%3A0x66a28e4f3c1fadc2!2s24+Page+Ave%2C+Wembley+HA9+9FP%2C+UK!5e0!3m2!1sen!2sin!4v1545470939892"></iframe>
					</div>
				</div>
			</div>

		<section class="mt40 mb40">
				<div class="container">

					<!-- Form + Sidebar -->
					<div class="row">
						<div class="col-sm-8">
							<div class="heading mb20"><h4><span class="ion-android-mail mr15"></span>Send us a Message</h4></div>
							<p class="mb20">Contact us for more details <b>if you leave outside the UK.</b></p>
							<form role="form" action="dropmsg.php" method="post">
			                    <div class="form-group">
			                        <input type="text" placeholder="Name" name="name" class="form-control" id="InputName">
			                    </div>
			                    <div class="form-group">
			                        <input type="email" placeholder="Email Address" name="email" class="form-control" id="InputEmail1">
			                    </div>
			                    <div class="form-group">
			                        <textarea class="form-control" placeholder="Message" name="msg" id="InputMessage" rows="7"></textarea>
			                    </div>
			                    <button type="submit" name="submit" class="btn btn-rw btn-primary">Submit</button>
			                </form>
						</div>
						<div class="col-sm-4 mt30-xs">
							<div class="content-box content-box-primary mb30">
						        <span class="ion-ios7-telephone-outline text-white border-white bordered-icon-static-sm mb10"></span>
						        <h2 class="text-white no-margin">(+44) 753369 5264</h2>
						    </div>
							<div class="panel panel-primary no-margin">
							    <div class="panel-heading">
							        <h3 class="panel-title"><span class="ion-android-system-home"></span> Information</h3>
							    </div>
							    <div class="panel-body">
							        <address class="no-margin">
			                            <strong>Alkebulan Saving Partners</strong><br>
			                            24 Page Avenue, <br>
			                            Wembley Middlesex HA9 9FP, UK<br>
			                            <abbr title="Phone">Tel:</abbr>(+44) 753369 5264 <br>
			                            Mail: <a href="#">admin@alkebulan.london,notfol@hotmail.com</a>
			                        </address>
							    </div>
							</div>
						</div>
					</div>

				</div><!-- /container -->
			</section>	
<?php

  include('footer.php');
?>