<?php
include_once('Mahmud_email.php');
include_once('Mahmud_utility.php');

function send_payment_mails($user_ids)
{
    
    $msg = array();
    //sending email to selected members
    foreach ($user_ids as $user_id) {

        $mail_sent = send_email_to_user($user_id);

        $msg[] = $mail_sent['message'];
    }
    return $msg;
}

function send_email_to_user($user_id)
{
    $me = new Mahmud_email();
    $mu = new Mahmud_utility();

    $user = $mu->getUser($user_id);

    $email_to = $user['email'];
    
    $mail_data['to'] = $email_to;
    $mail_data['subject'] = "Payment Failed";
    $mail_data['message'] = "Payment failed for {$user['fname']} {$user['lname']}";


    return $me->send($mail_data);

}

    ?>