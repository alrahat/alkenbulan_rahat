<?php

include_once('Mahmud_query.php');
Class Settings
{


    public $mq;
    var $table = 'settings';

    public function __construct()
    {
        $this->mq = new Mahmud_query();
    }

    public function setSettings($settings_type, $settings_key, $settings_val)
    {
        $settings_row = $this->getSettings($settings_type, $settings_key, $only_value = false);//check if the entire row exists

        if (empty($settings_row)) {
            $ins_data = array();
            $ins_data['settings_type'] = $settings_type;
            $ins_data['settings_key'] = $settings_key;
            $ins_data['settings_val'] = $settings_val;

            $this->mq->insert($this->table, $ins_data);
        }

        if (!empty($settings_row)) {
            $upd_data = array();
            $upd_data['settings_val'] = $settings_val;
            $this->mq->update($this->table,
                $upd_data," WHERE `settings_type`='$settings_type' AND `settings_key`='$settings_key' ");
        }
    }

    public function getSettings($type, $key, $only_value)
    {
        $ret = null;


        $sql = "SELECT * FROM `{$this->table}` where `settings_type`= '$type' AND `settings_key`= '$key' ";

        $row = $this->mq->row($sql);

        if (!empty($row)) {
            $ret = $row['settings_val'];
        }

        return $only_value ? $ret : $row;

    }

    public function getSectionSettings($type)
    {
        $section_settings = array();
        $section_settings_list = $this->getSettingsList($type);

        if (!empty($section_settings_list)) {
            foreach ($section_settings_list as $section_settings_list_item) {
                if (!empty($section_settings_list_item['settings_key'])) {
                    $section_settings[$section_settings_list_item['settings_key']] = $section_settings_list_item['settings_val'];
                }
            }
        }

        return $section_settings;
    }

    public function getSettingsList($type)
    {
        $sql = "SELECT * FROM `{$this->table}` where `settings_type`= '$type' ";

        return $this->mq->rows($sql);
    }


    public function get_stripe_test_mode()
    {
        $test_mode = 'on';
        $res = $this->getSettings('stripe', 'test_mode', $only_value = true);

        if (!empty($res)) {
            $test_mode = $res;
        }

        return $test_mode;
    }

    public function get_stripe_secret_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tsk' : 'lsk';
        return $this->getSettings('stripe', $key, $only_value = true);

    }

    public function get_stripe_public_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tpk' : 'lpk';
        return $this->getSettings('stripe', $key, $only_value = true);

    }


    public function default_stripe_settings()
    {
        $stripe_settings =array();
        $stripe_settings['test_mode'] = "";
        $stripe_settings['tsk'] = "";
        $stripe_settings['tpk'] = "";
        $stripe_settings['lsk'] = "";
        $stripe_settings['lpk'] = "";
        $stripe_settings['signing_secret'] = "";

        return $stripe_settings;
    }

    public function prepared_stripe_settings()
    {
        $original_stripe_settings = $this->getSectionSettings($settings_type = "stripe");

        $prepared_stripe_settings = $this->default_stripe_settings();

        if (!empty($original_stripe_settings)) {

            foreach ($original_stripe_settings as $original_stripe_setting_k => $original_stripe_setting_v) {
                $prepared_stripe_settings[$original_stripe_setting_k] = $original_stripe_setting_v;
            }

        }

        return $prepared_stripe_settings;
    }






}

?>
