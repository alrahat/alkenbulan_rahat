<?php
  include('header.php');
?>
        
      
   

<div class="container">
  <div class="col-md-10 " style="margin-top:60px; margin-bottom:60px;">
   <form class="form-horizontal" method="POST" action="changepassword.php?id=<?php echo $_SESSION['user_id'];?>" >
    <fieldset>

<!-- Form Name -->
<legend>Change Password</legend>

  <div class="form-group">
  <label class="col-md-4 control-label" for="Name (Full name)">Old Password</label>  
  <div class="col-md-4">
 <div class="input-group">
       <div class="input-group-addon">
        <i class="fa fa-lock">
        </i>
       </div>
       <input  name="oldpass" type="Password" placeholder="Old Password"  class="form-control input-md" required="required">
      </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Name (Full name)">New Password</label>  
  <div class="col-md-4">
 <div class="input-group">
       <div class="input-group-addon">
        <i class="fa fa-lock">
        </i>
       </div>
       <input  name="newpass" type="Password" placeholder="New Password"  class="form-control input-md" required="required">
      </div>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Name (Full name)">Confirm New Password</label>  
  <div class="col-md-4">
 <div class="input-group">
       <div class="input-group-addon">
        <i class="fa fa-lock">
        </i>
       </div>
       <input  name="cnewpass" type="Password" placeholder="Confirm New Password"  class="form-control input-md" required="required">
      </div>
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" ></label>  
  <div class="col-md-4">
  
  <input name="Submit" type="submit"  value="Change" class="btn btn-rw btn-primary button1">

    
  </div>
</div>

</fieldset>
  </form>
  </div>
</div>
<?php
  include('footer.php');
?>